
package test.pkg;
// java.util.Date;
// java.util.List;
// java.awt.List;

import java.lang.*;
import java.lang.String;
import java.util.Date;
import somepkg.SimpleClassName;
import java.util.*;
import java.awt.*;
import static java.lang.Math.PI;
import static java.lang.Math.*;

@HelloAnnotation(level=0) class HelloWorld {
/*
We have a class java.lang.Math
it has a static member called PI.
java.lang.Math.PI
*/
    public static void main(String[] args) {
        System.out.println("Hello world");
        List l1; // java.util.List
        String s1;  // test.pkg.String
        Date d1;    //  java.util.Date
//        SimpleClassName c1;
        System.out.println(cos(PI));
    }
}

class SomeClass {  // test.pkg.String
    int x;
    static int y;
    
}

/**
    This is a test class.
*/
class AnotherHelloWorld {
    /**
    
    */
    public static void main(String[] args) {
        System.out.println("Another Hello world");
        System.out.println("no. of args:"+args.length);
        for (String name : args) {
            System.out.println("Hello "+name);
        }
    }
    /**
    
    */
    public int main() {
        return 0;
    }
}

interface HelloInterface {

    public static void main(String[] args) {
        System.out.println("Hello interface");
        System.out.println("no. of args:"+args.length);
        for (String name : args) {
            System.out.println("Hello "+name);
        }
    }
}

enum HelloEnum { //   always has java.lang.Enum as the super-class
    HELLO,
    HI,
    ;
    // members of a class
    public static void main(String[] args) {
        System.out.println("Hello Enum");
        System.out.println("no. of args:"+args.length);
        for (String name : args) {
            System.out.println("Hello "+name);
        }
    }
}
// annotation
@interface HelloAnnotation {
    public abstract int level();

}
// record
record HelloRecord(/*String greeting*/) {

    public static void main(String[] args) {
        System.out.println("Hello record");
        System.out.println("no. of args:"+args.length);
        for (String name : args) {
            System.out.println("Hello "+name);
        }
        HelloRecord hr1 = new HelloRecord(/*"Hi"*/);
//        System.out.println(hr1.greeting());
    }
    
}
record Point(int x, int y) {
}





