
package util;

public enum ArithmeticOperator {
    ADD("+") {
        @Override
        public double compute(double a, double b) {
            return a + b;
        }
    },
    SUB("-") {
        @Override
        public double compute(double a, double b) {
            return a - b;
        }
    },
    MUL("*") {
        @Override
        public double compute(double a, double b) {
            return a * b;
        }
    },
    DIV("/") {
        @Override
        public double compute(double a, double b) {
            return a / b;
        }
    },
    MOD("%") {
        @Override
        public double compute(double a, double b) {
            return a % b;
        }
    },
;
    public abstract double compute(double a, double b);
    private String symbol;
    ArithmeticOperator(String s) {
        this.symbol = s;
    }
    public String getSymbol() {
        return this.symbol;
    }
    public static void main(String[] args) {
        String operName = args[0].toUpperCase();
        double a = Double.parseDouble(args[1]);
        double b = Double.parseDouble(args[2]);
        ArithmeticOperator oper = ArithmeticOperator.valueOf(operName);
        System.out.println(a + " " + oper.getSymbol() + " " + b + " = "
            + oper.compute(a,b));
    }
}

