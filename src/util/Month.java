
package util;

public enum Month {
    JAN("January"),    // ordinal value 0, name "JAN"
    FEB(29,"February") {
        @Override
        public String toString() {
            return super.toString()+" or 28";
        }
    },    // Month.FEB
    MAR("March"),
    APR(30,"April"),
    MAY("May"),
    JUN(30,"June"),
    JUL("July"),
    AUG("August"),
    SEP(30,"September"),
    OCT("October"),
    NOV(30,"November"),
    DEC("December"),
;
    private int maxDays = 31;
    private String fullName;
    private Month(int md, String name) {
        this.maxDays = md;
        this.fullName = name;
    }
    Month(String name) {
        this.fullName = name;
    }
    public int getMaxDays() {
        return this.maxDays;
    }
    @Override
    public String toString() {
        return this.fullName+" has "+this.maxDays;
    }
    public static void main(String[] args) {
        String monthName = args[0].toUpperCase();
        Month month = Month.valueOf(monthName);
        System.out.println(month);
    }
}

