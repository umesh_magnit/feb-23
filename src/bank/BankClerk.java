
package bank;

import java.util.regex.*;
import java.util.*;
import java.io.*;

public class BankClerk {
    private Bank bank;
    private Scanner commandScanner;
    private PrintWriter responseWriter;

    public BankClerk(Bank bank, InputStream in, OutputStream out) {
        this.bank = bank;
        this.commandScanner = new Scanner(in);
        this.responseWriter = new PrintWriter(out, true);
    }
    public BankClerk(Bank bank) {
        this(bank, System.in, System.out);
    }
    enum Command {
        OPEN("[Oo][Pp][Ee][Nn]\\s+(?<type>[SsCc])\\s+(?<name>\\w+)\\s+(?<openBal>\\d+)", 
            """
            Open <type> <name> <openBal>
               Open a new account, 
               type can be 'S' for Savings or 'C' for Current
            """),
        DEPOSIT("[Dd][Ee][Pp][Oo][Ss][Ii][Tt]\\s+(?<acno>\\d+)\\s+(?<amount>\\d+)", 
            """
            Deposit <acno> <amt>
                Deposit amount into the specified account
            """),
        WITHDRAW("[Ww][Ii][Tt][Hh][Dd][Rr][Aa][Ww]\\s+(?<acno>\\d+)\\s+(?<amount>\\d+)", "Withdraw <acno> <amt>"),
        TRANSFER("[Tt][Rr][Aa][Nn][Ss][Ff][Ee][Rr]\\s+(?<fromacno>\\d+)\\s+(?<toacno>\\d+)\\s+(?<amount>\\d+)", "Transfer <fromacno> <toacno> <amt>"),
        PASSBOOK("[Pp][Aa][Ss][Ss][Bb][Oo][Oo][Kk]\\s+(?<acno>\\d+)","Passbook <acno>"),
        DISPLAY("[Dd][Ii][Ss][Pp][Ll][Aa][Yy]\\s+(?<acno>\\d+)","Display <acno>"),
//        CLOSE("[Cc][Ll][Oo][Ss][Ee]\\s+(?<acno>\\d+)","Close <acno>"),
        LIST("[Ll][Ii][Ss][Tt]", "List"),
//        SAVE("[Ss][Aa][Vv][Ee]", "Save"),
        QUIT("[Qq][Uu][Ii][Tt]", "Quit"),
        HELP("[Hh][Ee][Ll][Pp]", "Help"),
        ;
        private Pattern syntax;
        private String helpString;
        Command(String syntax, String help) {
            this.syntax = Pattern.compile(syntax);
            this.helpString = help;
        }
        public Pattern getSyntax() {
            return this.syntax;
        }
        public String getHelpString() {
            return this.helpString;
        }
        public static Command getCommand(Matcher m) {
            for (Command command : Command.values()) {
                if (m.usePattern(command.getSyntax()).matches()) {
                    return command;
                }
            }
            return null;
        }
    }
    public void help() {
        responseWriter.println("valid commands are as follows:");
        for (Command command : Command.values()) {
            responseWriter.println(command.getHelpString());
        }
        responseWriter.println("enter a valid command");
    }
    public void run() {
        Matcher matcher = Pattern.compile("").matcher("");
        Command command = null;
        while ( (command = (Command.getCommand(
                    matcher.reset(
                        commandScanner.nextLine())))) 
                    != Command.QUIT) {
            if (command == null) {
                responseWriter.println("invalid command");
                help();
                continue;
            }
            switch(command) {
                default:
                    responseWriter.println("This command is not implemented yet");
                    continue;
//        OPEN("[Oo][Pp][Ee][Nn]\\s+(?<type>[SsCc])\\s+(?<name>\\w+)\\s+(?<openBal>\\d+)", "Open <type> <name> <openBal>"),
                case OPEN:
                    String typeString = null;
                    String name = null;
                    long openBal = 0;
                    long acno = 0;
                    try {
                        typeString = matcher.group("type");
                        name = matcher.group("name");
                        openBal = Long.parseLong(matcher.group("openBal"));
                        Bank.AccountType type = switch(typeString) {
                                case "S","s" -> Bank.AccountType.SAVINGS;
                                case "C","c" -> Bank.AccountType.CURRENT;
                                default -> null;
                            };
                        acno = bank.openAccount(type, name, openBal);
                        bank.display(acno);
                    } catch (Exception nae) {
                        responseWriter.println(nae);
                    }
                    continue;
//        DEPOSIT("[Dd][Ee][Pp][Oo][Ss][Ii][Tt]\\s+(?<acno>\\d+)\\s+(?<amount>\\d+)", "Deposit <acno> <amt>"),
                case DEPOSIT:
                    long amt = 0;
                    try {
                        acno = Long.parseLong(matcher.group("acno"));
                        amt = Long.parseLong(matcher.group("amount"));
                        bank.deposit(acno, amt);
                        bank.display(acno);
                    } catch (Exception ne) {
                        responseWriter.println(ne);
                    }
                    continue;
//        WITHDRAW("[Ww][Ii][Tt][Hh][Dd][Rr][Aa][Ww]\\s+(?<acno>\\d+)\\s+(?<amount>\\d+)", "Withdraw <acno> <amt>"),
                case WITHDRAW:
                    try {
                        acno = Long.parseLong(matcher.group("acno"));
                        amt = Long.parseLong(matcher.group("amount"));
                        if (!bank.withdraw(acno, amt)) {
                            responseWriter.println("insufficient balance");
                        }
                        bank.display(acno);
                    } catch (Exception ne) {
                        responseWriter.println(ne);
                    }
                    continue;
//        TRANSFER("[Tt][Rr][Aa][Nn][Ss][Ff][Ee][Rr]\\s+(?<fromacno>\\d+)\\s+(?<toacno>\\d+)\\s+(?<amount>\\d+)", "Transfer <fromacno> <toacno> <amt>"),
                case TRANSFER:
                    try {
                        long fromacno = Long.parseLong(matcher.group("fromacno"));
                        long toacno = Long.parseLong(matcher.group("toacno"));
                        amt = Long.parseLong(matcher.group("amount"));
                        if (!bank.transfer(fromacno, toacno, amt)) {
                            responseWriter.println("insufficient balance");
                        }
                        bank.display(fromacno);
                        bank.display(toacno);
                    } catch (NoSuchAccountException ne) {
                        responseWriter.println("invalid account"+ne);
                    } catch (Exception ne) {
                        responseWriter.println(ne);
                    }
                    continue;
//        PASSBOOK("[Pp][Aa][Ss][Ss][Bb][Oo][Oo][Kk]\\s+(?<acno>\\d+)","Passbook <acno>"),
                case PASSBOOK:
                    try {
                        acno = Long.parseLong(matcher.group("acno"));
                        bank.printPassbook(acno);
                    } catch (Exception ne) {
                        responseWriter.println(ne);
                    }
                    continue;
//        DISPLAY("[Dd][Ii][Ss][Pp][Ll][Aa][Yy]\\s+(?<acno>\\d+)","Display <acno>"),
                case DISPLAY:
                    try {
                        acno = Long.parseLong(matcher.group("acno"));
                        bank.display(acno);
                    } catch (Exception ne) {
                        responseWriter.println(ne);
                    }
                    continue;
//        CLOSE("[Cc][Ll][Oo][Ss][Ee]\\s+(?<acno>\\d+)","Close <acno>"),
/*
                case CLOSE:
                    try {
                        acno = Long.parseLong(matcher.group("acno"));
                        long closeBal = bank.closeAccount(acno);
                        responseWriter.println("closed account "+acno+" with balance "+closeBal);
                    } catch (Exception ne) {
                        responseWriter.println(ne);
                    }
                    continue;
*/
//        LIST("[Ll][Ii][Ss][Tt]", "List"),
                case LIST:
                    bank.listAccounts();
                    continue;
/*
                case SAVE:
                    try {
                        bank.save();
                        responseWriter.println("saved successfully");
                    } catch (IOException ioe) {
                        ioe.printStackTrace(responseWriter);
                    }
                    continue;
*/
//        HELP("[Hh][Ee][Ll][Pp]", "Help"),
                case HELP:
                    help();
                    continue;
/*
        QUIT("[Qq][Uu][Ii][Tt]", "Quit"),
*/                    
            }
        }
    }

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        String bankName = args[0];
        Bank bank = null;
        int bankCode = Integer.parseInt(args[1]);
        bank = new Bank(bankName, bankCode);
        BankClerk clerk = new BankClerk(bank);
        clerk.run();
    }
}

