
package bank;

@FunctionalInterface
public interface Penalty {
    public long compute(long minBal, long bal);
    public static final Penalty DEFAULT = (minbal, bal) -> bal < minbal ? 500 : 0;
    
}

