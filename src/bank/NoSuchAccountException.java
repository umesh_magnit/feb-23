
package bank;

public class NoSuchAccountException extends Exception {
    private long accountNumber;
    NoSuchAccountException(String msg, long acno) {
        super(msg);
        this.accountNumber = acno;
    }
    public long getAccountNumber() {
        return this.accountNumber;
    }
    @Override
    public String toString() {
        return super.toString()+":"+this.accountNumber;
    }
}

