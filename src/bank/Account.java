
package bank;

import java.time.LocalDateTime;

public abstract class Account {
    private static final int INITIAL_PASSBOOK_SIZE = 100;
    private static long lastAccountNumber = 10000;

    private long accountNumber;
    private String name;
    private long balance;
    private Transaction[] passbook = new Transaction[INITIAL_PASSBOOK_SIZE];
    private int nextTransactionIndex = 0;

    protected Account(long acno, String name, long openBal) throws NegativeAmountException {
        this.accountNumber = acno;
        this.name = name;
        new Transaction(TransType.OPEN, openBal);
    }
    protected Account(String name, long openBal) throws NegativeAmountException {
        this(++lastAccountNumber, name, openBal);
    }
    class Transaction { // inner class
        private LocalDateTime dateTime = LocalDateTime.now();
        private TransType type;
        private long amount;
        Transaction(TransType type, long amt) throws NegativeAmountException {
            if (amt < 0) {
                throw new NegativeAmountException("negative "+type.getNaration(), amt);
            }
            this.type = type;
            this.amount = amt;
            Account.this.balance += this.getNetAmount();
            Account.this.passbook[nextTransactionIndex++] = this;
        }
        public LocalDateTime getDateTime() {
            return this.dateTime;
        }
        public TransType getType() {
            return this.type;
        }
        public long getAmount() {
            return this.amount;
        }
        public String getNaration() {
            return this.type.getNaration();
        }
        public long getNetAmount() {
            return this.type.getSign()*this.amount;
        }
    }
    public enum TransType {
        OPEN(1,"Opening Balance"),
        DEPOSIT(1,"Deposit"),
        WITHDRAWAL(-1,"Withdrawal"),
        PENALTY(-1,"Penalty"),
        ;
        private int sign;
        private String naration;
        TransType(int sign, String naration) {
            this.sign = sign;
            this.naration = naration;
        }
        public int getSign() {
            return this.sign;
        }
        public String getNaration() {
            return this.naration;
        }
    }
    public long getAccountNumber() {
        return this.accountNumber;
    }
    public String getName() {
        return this.name;
    }
    public long getBalance() {
        return this.balance;
    }
    
    public String toString() {
        return this.getClass().getSimpleName()+":"
                +this.getAccountNumber()
                +", "+this.getName()
                +", "+this.getBalance()
                ;
    }
    public Transaction[] getPassbook() {
    // update the return statement below
        return null;
    }
    public void display() {
        System.out.println(this);
    }
    public void printPassbook() {
        System.out.println("Passbook of "+this.name);
        System.out.printf("Date and Time       %-15s %12s %12s %12s\r\n",  
            "Naration", "Withdrawals", "Deposits", "Balance");
        long runningBalance = 0;
        for (Transaction t : this.passbook) {
            runningBalance += t.getNetAmount();
            System.out.printf("%tF %tT %-15s %12d %12d %12d\r\n",  
                t.getDateTime(),
                t.getDateTime(),
                t.getNaration(),
                t.getType().getSign() == -1 ? t.getAmount() : 0,
                t.getType().getSign() == 1 ? t.getAmount() : 0,
                runningBalance
                );
        }
        System.out.println("End of passbook");
    }
    public void deposit(long amt) throws NegativeAmountException {
        new Transaction(TransType.DEPOSIT, amt);
    }
    public boolean withdraw(long amt) throws NegativeAmountException {
        if (this.balance < amt) {
            return false;
        }
        new Transaction(TransType.WITHDRAWAL, amt);
        return true;
    }
}

