
package bank;

public class SavingsAccount extends Account {
    public SavingsAccount(long acno, String name, long openBal) throws NegativeAmountException {
        super(acno, name, openBal);
    }
    public SavingsAccount(String name, long openBal) throws NegativeAmountException {
        super(name, openBal);
    }
}

