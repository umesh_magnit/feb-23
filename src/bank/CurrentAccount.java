
package bank;

public class CurrentAccount extends Account {
    private static final long minimumBalance = 10_000;
    private Penalty penalty = Penalty.DEFAULT;
    public CurrentAccount(long acno, String name, long openBal) throws NegativeAmountException {
        super(acno, name, openBal);
    }
    public CurrentAccount(String name, long openBal) throws NegativeAmountException {
        super(name, openBal);
    }
    @Override
    public boolean withdraw(long amt) throws NegativeAmountException {
        if (!super.withdraw(amt)) {
            return false;
        }
        if (this.getBalance() < minimumBalance) {
            new Transaction(TransType.PENALTY, this.penalty.compute(minimumBalance, this.getBalance()));
        }
        return true;
    }
}

