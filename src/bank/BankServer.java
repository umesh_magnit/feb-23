
package bank;

import java.net.ServerSocket;
import java.net.Socket;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.IOException;
import java.io.File;

public class BankServer {
    private Bank bank;
    private ServerSocket board;
    public BankServer(Bank bank, int port) throws IOException {
        this.bank = bank;
        this.board = new ServerSocket(port);
    }
    public void run() throws IOException {
        while (true) {
            Socket phone = board.accept();
            InputStream receiver = phone.getInputStream();
            OutputStream transmitter = phone.getOutputStream();
            BankClerk clerk = new BankClerk(this.bank, receiver, transmitter);
            clerk.run();
        }
    }
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        int port = Integer.parseInt(args[0]);
        String bankName = args[1];
        Bank bank = null;
        int bankCode = Integer.parseInt(args[2]);
        bank = new Bank(bankName, bankCode);
        BankServer server = new BankServer(bank, port);
        server.run();
    }
}

