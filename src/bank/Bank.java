
package bank;

public class Bank {
    private String name;
    private long nextAccountNumber;
    private int bankCode;   //  bankCodexxxx
    private Account[] accounts = new Account[10_000];
    public Bank(String name, int bankCode) {
        this.name = name;
        this.bankCode = bankCode;
        this.nextAccountNumber = bankCode * 10_000L;
    }
    public enum AccountType {
        SAVINGS {
            @Override
            public Account create(long acno, String name, long openBal) throws NegativeAmountException{
                return new SavingsAccount(acno, name, openBal);
            }
        },
        CURRENT {
            @Override
            public Account create(long acno, String name, long openBal) throws NegativeAmountException{
                return new CurrentAccount(acno, name, openBal);
            }
        },
        ;
        public abstract Account create(long acno, String name, long openBal) throws NegativeAmountException;
    }
    public long openAccount(AccountType type, String name, long openBal) throws NegativeAmountException {
        Account ac = type.create(nextAccountNumber++, name, openBal);
        this.accounts[(int)(ac.getAccountNumber() % 10_000)] = ac;
        return ac.getAccountNumber();
    }
    private Account getAccount(long acno) throws NoSuchAccountException {
        if (acno < this.bankCode*10_000L) {
            throw new NoSuchAccountException("invalid acno", acno);
        }
        if (acno >= this.nextAccountNumber) {
            throw new NoSuchAccountException("invalid acno", acno);
        }
        return accounts[(int)(acno % 10_000)];
    }
    /**

        @param acno - the account number in which to depost
        @param amt  - the amount to be deposited 
    */
    public void deposit(long acno, long amt) throws NegativeAmountException, NoSuchAccountException {
        getAccount(acno).deposit(amt);
    }
    public boolean withdraw(long acno, long amt) throws NegativeAmountException, NoSuchAccountException {
        return getAccount(acno).withdraw(amt);
    }
    public void printPassbook(long acno) throws NoSuchAccountException {
        getAccount(acno).printPassbook();
    }
    public void display(long acno) throws NoSuchAccountException {
        getAccount(acno).display();
    }
    public boolean transfer(long fromAcno, long toAcno, long amt) throws NegativeAmountException, NoSuchAccountException {
        Account fromAccount = getAccount(fromAcno);
        Account toAccount = getAccount(toAcno);
        if (!fromAccount.withdraw(amt)) {
            return false;
        }
        toAccount.deposit(amt);
        return true;
    }
    public void listAccounts() {
        System.out.println("List of accounts for bank "+this.name);
        // if accounts was a List<Account>
        // accounts.forEach(ac -> ac.display());        
        // accounts.forEach(Account::display);        
        
        for (int i = 0; i < nextAccountNumber % 10_000; i++) {
            accounts[i].display();
        }
        System.out.println("End of List");
    }
}

